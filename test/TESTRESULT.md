**Version**: 1.0.0

**Date**: 2020-08-12

**Performed by**: Gabriel Fedel and Karl Vestin

**Test setup**: Tested using the CAENelS FAST-PS 2020-400 in rack 3 in the server room. Connected to the lab network.

IOC Test
========
The following tests are conducted using manual CA commands from promt.


| Test case            | Result | Comment                                                                 |
| -------------------- | ------ | ----------------------------------------------------------------------- |
| Basic connection     | OK | Could connect to the PS on IP 172.304.189 without problems             | 
| CAGet on Vol-R     | OK | Reading 0 V             |
| CAGet on Cur-R     | OK | Reading 0 A             |
| CAGet on Sts-R     | OK | Reading 0               |
| CAGet on Model-R   | OK | Reading FAST-PS         |
| CAGet on GndCur-R  | OK | Reading 0 A             |
| CAGet on DCLnkVol-R     | OK | Reading around 23.8 V             |
| CAGet on HeatSnkTmp-R     | OK | Reading around 33.5 C             |
| CAGet on CurrentPosPolatity     | OK | Reading 0             |
| CAGet on CurrentNegPolatity     | OK | Reading 0            |
| CAPut Vol-S to 10V  | OK | Power supply #NAK13 since it is OFF |
| CAPut Pwr-S to On | OK | PS turns on |
| CAPut Cur-S to 1 A | OK | PS goes into regulation fault (since no load connected) |
| CAPut Rst to 1 | OK | PS restes and clears fault |
| CAPut Pwr-S to On | OK | PS turns on |
| CAPut Vol-S to 1V | OK | PS responds NAK20 since it is in constant current mode |
| CAMonitor on Vol-R | OK | Reads the voltage correctly around 2.18 V |
| CAMonitor on Cur-R | OK | Reads the voltage correctly around 0 A |
| CAMonitor on GndCur-R  | OK | Reading 0 A             |
| CAGet on Sts-R     | OK | Reading 1 (since the PS is now ON)               
| CAPut Pwr-S to Off | OK | PS turns off |

OPI test
========
The following tests are conducted in CS Studio using the OPI from this repository.

| Test case            | Result | Comment                                                                 |
| -------------------- | ------ | ----------------------------------------------------------------------- |
| Turn device ON     | OK | Device turns on as expected             | 
| Verify that device is in constant current mode     | OK | Device is in constant current mode             | 
| Verify that device is in remote control mode     | OK | Device is in remote control mode             | 
| Voltage setpoint     | OK | Reads correctly (should not be possible to change)             | 
| Voltage read back    | OK | Reads correctly             | 
| Current setpoint     | OK | Reads and writes correctly             | 
| Current readback     | OK | Reads correctly             | 
| Set current to 1A, then verify that "regulation fault" occurs     | OK | Fault occurs as expected             | 
| Press reset and verify that fault is cleared and PS turned "OFF"     | OK | Fault cleared, device OFF             | 
